import grpc from "@grpc/grpc-js";
import { loadSync } from "@grpc/proto-loader";
import pkg from "google-protobuf";

const { google } = pkg;

const packageDefinition = loadSync("./database.proto", {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const { database } = grpc.loadPackageDefinition(packageDefinition);

const client = new database.DatabaseService("localhost:5000", grpc.credentials.createInsecure());

const payloads = [
  { idKartuAkses: "31313131", idRegisterGate: "45", isValid: true },
  { idKartuAkses: "31313131", idRegisterGate: "46", isValid: true },
];

payloads.forEach((payload, index) => {
  client.insertLog(payload, (error, response) => {
    if (error) {
      console.error(`Error publishing message ${index + 1}:`, error);
    } else {
      console.log(`Published message ${index + 1} to server`);
      console.log("Response:", response);
    }
  });
});
