import grpc from "@grpc/grpc-js";
import { loadSync } from "@grpc/proto-loader";

const packageDefinition = loadSync("./database.proto", {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const { database: { DatabaseService } } = grpc.loadPackageDefinition(packageDefinition);

// Sisa kode server.js
const server = new grpc.Server();

server.addService(DatabaseService.service, {
  insertLog: async (call, callback) => {
    const { tableName, idKartuAkses, idRegisterGate, isValid } = call.request;
    try {
      await insertLog(tableName, idKartuAkses, idRegisterGate, isValid);
      callback(null, { success: true });
    } catch (error) {
      callback(error);
    }
  },
  goQuery: async (call, callback) => {
    const { query } = call.request;
    try {
      const result = await goQuery(query);
      callback(null, { result });
    } catch (error) {
      callback(error);
    }
  },
});

server.bindAsync(
  "localhost:5000",
  grpc.ServerCredentials.createInsecure(),
  (error, port) => {
    if (error) {
      console.error("Failed to bind gRPC server:", error);
    } else {
      console.log(`gRPC server is running on port ${port}`);
      server.start();
    }
  }
);
